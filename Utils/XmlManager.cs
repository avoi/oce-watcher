﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace FileWatcherManager.Utils
{
    static class XmlManager
    {
        //TODO refactor targetPath to service configuration file 
        static string targetPath = "watchers.xml";

        private static List<FileSystemWatcher> fsWatchers = new List<FileSystemWatcher>();

        public static void SaveXml(List<FileSystemWatcher> fsWatchers)
        {
            var xEle = new XElement("watchers",
                from a in fsWatchers
                select new XElement("path", a.Path));

            xEle.Save(targetPath);
        }

        //TODO called at service startup
        public static IEnumerable<String> LoadXml()
        {
            try
            { 
                XDocument doc = XDocument.Load(targetPath);

                var list = doc.Root.Elements("path")
                           .Select(element => element.Value)
                           .ToList();

                return list;
            }
            /*if (File.Exists(fileName)) ...is a race condition and always wrong
            Instead, just try loading the document and catch the exception.*/
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.Message);
                return new List<string>();
            }

        }


    }
}
