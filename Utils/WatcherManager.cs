﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileWatcherManager.Utils
{
    static class WatcherManager
    {
        //TODO refactor WatcherManager to Singleton lifetime service
        static List<FileSystemWatcher> fsWatchers = new List<FileSystemWatcher>();

        public static List<KeyValuePair<string, string>> watchersLog = new List<KeyValuePair<string, string>>();

        public static void AddWatchers(IEnumerable<string> wathersList)
        {
            foreach (string path in wathersList)
            {
                AddWatcher(path);
            }
        }

        public static void AddWatcher(string path)
        {
            FileSystemWatcher fsw = new FileSystemWatcher();
            fsw.Path = path;
            fsw.Changed += OnChanged;
            fsw.Created += OnChanged;
            fsw.Deleted += OnChanged;
            fsw.Renamed += OnRenamed;
            fsw.EnableRaisingEvents = true;

            /*TODO safetly dispose objects when removed
            fsw.EnableRaisingEvents = false 
            unsubscribe from events: fsw-= OnChanged; etc... */
            fsWatchers.Add(fsw);

            //TODO refactor XmlManager as singleton lifetime service 
            XmlManager.SaveXml(fsWatchers);
        }

        static void OnChanged(object source, FileSystemEventArgs e)
        { 
            string message = ($"Nume folder: {new FileInfo(e.FullPath).Directory.Name}" +
                Environment.NewLine + $"Nume fisier: {new FileInfo(e.FullPath).Name}" +
                Environment.NewLine + $"Nume eveniment: {e.ChangeType} " +
                Environment.NewLine + $"Timestamp: {DateTime.Now}");
        
            Debug.WriteLine(message);
            watchersLog.Add(new KeyValuePair<string, string>(Path.GetDirectoryName(e.FullPath), message));
        }

        static void OnRenamed(object source, RenamedEventArgs e)
        { 
            string message = ($"Nume folder: {new FileInfo(e.FullPath).Directory.Name}" +
                Environment.NewLine + $"Nume fisier: {new FileInfo(e.OldFullPath).Directory.Name} " +
                Environment.NewLine + $"Redenumit in: {new FileInfo(e.FullPath).Name} " +
                Environment.NewLine + $"Timestamp: {DateTime.Now}");

            Debug.WriteLine(message);
            watchersLog.Add(new KeyValuePair<string, string>(Path.GetDirectoryName(e.FullPath), message));
        }
    }
}
