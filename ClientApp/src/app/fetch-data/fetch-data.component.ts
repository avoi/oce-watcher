import { Component, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public watchersList: any;
  public http: HttpClient;
  public baseUrl: string;
  public params: HttpParams | { [param: string]: string | string[]; };

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http = http;
    this.baseUrl = baseUrl;

    http.get<any[]>(baseUrl + 'api/SampleData/ListWatchers').subscribe(result => {
      this.watchersList = result;
    }, error => console.error(error));
  }

  public showLog(path: any) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    this.params = new HttpParams().set("path", path);

    const requestOptions = {
      params: this.params
    };

    this.http.get<any[]>(this.baseUrl + 'api/SampleData/Events', requestOptions).subscribe(result => {
      //TODO add modal window
      alert(result);
    }, error => console.error(error));
  }

}
