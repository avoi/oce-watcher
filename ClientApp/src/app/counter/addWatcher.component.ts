import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-addWatcher-component',
  templateUrl: './addWatcher.component.html'
})
export class AddWatcherComponent {
  public filePath: string;

  constructor(private httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) {
    this.filePath;
  }

  public addWatcher() {
    this.httpClient.post(this.baseUrl + "api/SampleData/addWatcher",
      {
        "path": this.filePath
      })
      .subscribe(
        data => {
          console.log("POST Request is successful ", data);
          alert("Successfully added path: ");
          this.ClearPath();
        },
        error => {
          console.log("Error", error);
          alert(error.error.text);
          this.ClearPath();
        }
      );
  }

  public ClearPath() {
    this.filePath = "";
  }

}
