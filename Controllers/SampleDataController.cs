using FileWatcherManager.Utils;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

//TODO refactor to Console app
namespace FileWatcher.Controllers
{
    //TODO reafctor to asynchronous controller
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
       
        [HttpPost("[action]")]
        public ActionResult AddWatcher([FromBody] JObject json)
        {
            try
            {
                WatcherManager.AddWatcher(json["path"].ToString());
                return Ok();
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public IEnumerable<string> ListWatchers()
        {
            return XmlManager.LoadXml();
        }

        [HttpGet("[action]")]
        public IEnumerable<string> Events(string path)
        {
            return WatcherManager.watchersLog.Where(kvp => kvp.Key == Path.GetFullPath(path))
                .Select(kvp => kvp.Value)
                .Take(10)
                .ToList();
        }
    }
}
