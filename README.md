1. Creati o aplicatie consola care va monitoriza un folder introdus de la tastatura. Aplicatia va
afisa evenimentele pe ecran sub forma:

Nume folder – Nume fisier – nume eveniment – timestamp

Se urmaresc doar urmatoarele evenimente : creare, stergere si redenumire fisier.

2. Pornind de la aplicatia anterioara, se cere sa se creeze o aplicatie web care sa poata
monitoriza independent o lista de foldere. Aplicatia va avea urmatoarea interfata:

a. O lista care va contine directoarele de monitorizat
b. Un buton care permite adaugarea unui nou director pentru monitorizare
c. Un buton care permite stergerea unui director de la monitorizare
d. O lista care va afisa ultimele 10 evenimente pe folderul selectat. Afisarea se face la
fel ca la punctul 1.

Se cere ca lista de directoare sa se persiste intr-un fisier XML.